﻿using Extensions;
using Gameplay;
using UnityEngine;

namespace Game.Scripts.UI.ProgressBars
{
	public class HealthBar : ProgressBar
	{
		[SerializeField, GroupSceneObject] private Health _health;

		private void OnEnable()
		{
			_health.OnInit += Init;
			_health.OnChange += Change;
		}

		private void OnDisable()
		{
			_health.OnInit += Init;
			_health.OnChange += Change;
		}

		private void Init(float maxValue)
		{
			SetMaxValue(maxValue, true);
			SetValue(maxValue, true);
		}

		private void Change(float currentValue)
		{
			SetValue(currentValue);
		}
	}
}