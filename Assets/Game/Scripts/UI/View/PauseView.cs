﻿using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI.View
{
	public class PauseView : MonoBehaviour
	{
		[SerializeField, GroupComponent] private CanvasGroup _canvasGroup;
		[SerializeField, GroupComponent] private Button _exitPauseButton;
		[SerializeField, GroupSetting] private float _showAndHideDuration = 0.5f;

		public Button ExitPauseButton => _exitPauseButton;

		public void OpenPauseMenu(Action action) =>
			_canvasGroup.Show(_showAndHideDuration, callback: action);

		public void ClosePauseMenu(Action action) => 
			_canvasGroup.Hide(_showAndHideDuration, callback: action);
	}
}