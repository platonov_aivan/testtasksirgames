﻿using DG.Tweening;
using Extensions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI.View
{
	public class GameView : MonoBehaviour
	{
		[SerializeField, GroupComponent] private Button _pauseButton;
		[SerializeField, GroupComponent] private TextMeshProUGUI _moneyText;
		[SerializeField, GroupComponent] private TextMeshProUGUI _levelText;
		[SerializeField, GroupComponent] private TextMeshProUGUI _timerText;
		[SerializeField, GroupSetting] private float _showTextDuration = 2f;
		[SerializeField, GroupSetting] private float _hideTextDuration = 0.2f;
		
		public Button PauseButton => _pauseButton;
		
		public void SetMoneyText(int countMoney)
		{
			_moneyText.text = $"Money : {countMoney}";
		}

		public void SetLevelText(int levelIndex)
		{
			_levelText.text = $"Level : {levelIndex + 1}";

			_levelText.DOKill();
			_levelText.DOColor(Color.white, _showTextDuration)
				.SetLink(gameObject)
				.OnComplete(() =>
					_levelText.DOColor(Color.clear, _hideTextDuration).SetLink(gameObject)
				);
		}

		public void SetTimerText(float time)
		{
			var currentTime = Mathf.CeilToInt(time);
			_timerText.text = $"Start in {currentTime} sec";
		}

		public void EnableTimeText(bool active)
		{
			_timerText.DOKill();

			if (active)
			{
				_timerText.DOColor(Color.white, _hideTextDuration).SetLink(gameObject);
			}
			else
			{
				_timerText.DOColor(Color.clear, _hideTextDuration).SetLink(gameObject);
			}
		}
	}
}