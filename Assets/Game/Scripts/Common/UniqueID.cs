using Sirenix.OdinInspector;
using UnityEngine;

namespace Common
{
	public class UniqueID : MonoBehaviour
	{
		[SerializeField, ReadOnly] private string _id;

		public string ID
		{
			get => _id;
			set => _id = value;
		}
	}
}