﻿using System;

namespace Common
{
	public static class Enums
	{
		public enum Axis
		{
			X,
			Y,
			Z
		}

		public enum AdsResult
		{
			None,
			Success,
			NotAvailable,
			Start,
			Fail,
			Watched,
			Canceled,
			Clicked
		}

		public enum ResourceType
		{
			Money,
			Resource2,
			Resource3,
		}

		public enum UpgradeType
		{
			Upgrade1,
			Upgrade2,
			Upgrade3,
		}

		public enum AIState
		{
			GetResource,
			Waiting,
			Payment,
			Exit
		}

		public enum ResourceCarryType
		{
			Default,
			Follow
		}

		public enum ParticleType
		{
			None = 0,
			Poof = 1,
			GunFire = 2,
			Hit = 3,
			SmokeWhite = 4,
			SmokeDark = 5,
			Death = 6,
			Upgrade = 7,
			Trail = 8,
			Explosion = 9,
		}
		
		public enum BulletType
		{
			None = 0,
			Ball = 1,
			Arrow = 2
		}

		public enum TeamType
		{
			None = 0,
			Player = 1,
			Enemy = 2,
		}

		public enum ActionType
		{
			Idle = 0,
			RandomMove = 1,
			MoveToPlayer = 2,
		}

		public enum CharacterType
		{
			None = 0,
			Player = 1 << 0,
			MonsterLand = 1 << 1,
			MonsterFly = 1 << 2,
		}

		public enum MonsterType
		{
		}
	}
}