﻿namespace Common
{
	public static class Constants
	{
		public static class AssetPath
		{
			public const string SO_DATA_PATH   = "Game/ScriptableObjects";
			public const string POOL_ITEM_PATH = "Assets/Game/Prefabs|Assets/Game/VFX";
		}
	}
}