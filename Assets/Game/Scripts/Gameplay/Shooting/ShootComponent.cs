﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.ObjectPool;
using Extensions;
using Game.Scripts.Gameplay.Characters;
using Game.Scripts.Gameplay.Characters.Enemies;
using Game.Scripts.Gameplay.Interfaces;
using Game.Scripts.Gameplay.Providers;
using UnityEngine;

namespace Game.Scripts.Gameplay.Shooting
{
	public class ShootComponent : MonoBehaviour
	{
		public event Action OnStartShoot;
		public event Action OnStopShoot;

		[SerializeField] private CharacterBase _characterBase;

		[GroupView] private readonly List<IDamageable> _enemyList = new List<IDamageable>();

		private readonly RaycastHit[] _raycastHits = new RaycastHit[10];
		
		public void ClearEnemy()
		{
			_enemyList.Clear();
		}

		public IDamageable GetCloseEnemy()
		{
			var nearest = _enemyList.Aggregate(
				(x, y) => (x.ModelPosition - _characterBase.ModelPosition).sqrMagnitude <
				          (y.ModelPosition - _characterBase.ModelPosition).sqrMagnitude &&
				          IsAvailableForShot(x)
					? x
					: y);

			if (nearest == default || !IsAvailableForShot(nearest)) return default;

			return nearest;
		}

		public void Shoot(Transform target)
		{
			var bulletPrefab = PrefabProvider.GetBulletPrefab(_characterBase.WeaponData.BulletType);
			var bullet = Pool.Get(bulletPrefab, _characterBase.BulletSpawnPos.position);
			bullet.Init(_characterBase.WeaponData, target, _characterBase.Data.Team);
		}

		private bool IsAvailableForShot(IDamageable damageable)
		{
			Ray ray = new Ray(_characterBase.ModelPosition, damageable.ModelPosition - _characterBase.ModelPosition);

			var minDistance = float.MaxValue;
			RaycastHit hit = default;
			var count = Physics.RaycastNonAlloc(ray, _raycastHits, _characterBase.Data.MaxDistance,
				_characterBase.Data.RayLayerMask);

			if (count <= 0) return false;

			for (var i = 0; i < count; i++)
			{
				if (_raycastHits[i].distance < minDistance)
				{
					hit = _raycastHits[i];
					minDistance = hit.distance;
				}
			}

			var isEnemy = hit.collider.TryGetComponent(out Enemy _);

#if UNITY_EDITOR
			Debug.DrawRay(ray.origin, ray.direction * hit.distance);
#endif

			return isEnemy;
		}

		private void OnEnable()
		{
			_characterBase.FarZone.OnZoneEnter += OnEnterFarZone;
			_characterBase.FarZone.OnZoneExit += OnExitFarZone;
		}

		private void OnDisable()
		{
			_characterBase.FarZone.OnZoneEnter -= OnEnterFarZone;
			_characterBase.FarZone.OnZoneExit -= OnExitFarZone;
		}


		private void OnEnterFarZone(Collider other)
		{
			if (other.transform.TryGetComponent<IDamageable>(out var enemy)) AddEnemy(enemy);
		}

		private void OnExitFarZone(Collider other)
		{
			if (other.transform.TryGetComponent<IDamageable>(out var enemy)) RemoveEnemy(enemy);
		}


		private void AddEnemy(IDamageable enemy)
		{
			if (!_enemyList.Contains(enemy)) _enemyList.Add(enemy);

			enemy.OnDie += RemoveEnemy;
			OnStartShoot?.Invoke();
		}

		private void RemoveEnemy(IDamageable enemy)
		{
			enemy.OnDie -= RemoveEnemy;
			_enemyList.Remove(enemy);
			if (_enemyList.Count != 0) return;

			OnStopShoot?.Invoke();
		}
	}
}