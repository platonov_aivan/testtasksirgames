﻿using Cinemachine;
using Game.Scripts.Levels;
using UnityEngine;

namespace Game.Scripts.Gameplay.Cameras
{
	public class CameraHandler : MonoBehaviour
	{
		[SerializeField] private CinemachineVirtualCamera _cinemachineVirtual;
		
		private void Awake()
		{
			Level.OnTargetGroupFollow += SetFollowGroup;
		}

		private void OnDestroy()
		{
			Level.OnTargetGroupFollow -= SetFollowGroup;
		}

		private void SetFollowGroup(CinemachineTargetGroup targetGroup)
		{
			_cinemachineVirtual.Follow = targetGroup.transform;
		}
	}
}
