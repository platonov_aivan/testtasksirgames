﻿using System;
using System.Collections;
using Extensions;
using Game.ScriptableObjects.Classes.GameSession;
using Game.Scripts.Gameplay.Characters.Enemies;
using Game.Scripts.Gameplay.Interfaces;
using Game.Scripts.Gameplay.LevelGrid;
using Game.Scripts.Gameplay.Spawner;
using UnityEngine;

namespace Game.Scripts.Gameplay
{
	public class GameSession : MonoBehaviour
	{
		public event Action OnStartBattle;
		public event Action OnAllMonstersDied;
		public event Action OnPlayerDied;
		public event Action<int> OnMoneyChanged;
		public event Action<float> OnTimerChanged;

		[SerializeField, GroupComponent] private FieldGrid _fieldGrid;
		[SerializeField, GroupComponent] private CharacterSpawner _characterSpawner;
		[SerializeField, GroupAssets] private GameSessionData _gameSessionData;

		[GroupView] private int _currentMoney;
		[GroupView] private float _currentTime;

		public int StartTime => _gameSessionData.StartTime;

		public int CurrentMoney
		{
			get => _currentMoney;
			set
			{
				_currentMoney = value;
				OnMoneyChanged?.Invoke(_currentMoney);
			}
		}

		private Coroutine _startTimeBattleCor;

		public void Spawn()
		{
			_characterSpawner.SpawnPlayer(_fieldGrid.PlayerSpawnPos);
			_characterSpawner.SpawnMonsters(_fieldGrid.GetRandomPos, _gameSessionData.CountMonsters);

			StartTimeBattle();
		}

		private void StartTimeBattle()
		{
			_currentTime = 0f;
			_startTimeBattleCor.Stop(this);
			_startTimeBattleCor = StartCoroutine(StartTimeBattleCor());
		}


		private IEnumerator StartTimeBattleCor()
		{
			while (_currentTime <= _gameSessionData.StartTime)
			{
				yield return null;
				_currentTime += Time.deltaTime;
				OnTimerChanged?.Invoke(_gameSessionData.StartTime - _currentTime);
			}

			StartBattle();
			OnStartBattle?.Invoke();
		}

		private void StartBattle()
		{
			_characterSpawner.PlayerInLevel.OnDie += PlayerDied;
			_characterSpawner.PlayerInLevel.InitMovement();

			foreach (var enemy in _characterSpawner.MonstersInLevel)
			{
				enemy.OnDie += DieHandler;
				enemy.InitMovement();
				enemy.StartAction();
			}
		}

		private void PlayerDied(IDamageable player)
		{
			player.OnDie -= PlayerDied;

			OnPlayerDied?.Invoke();
		}

		private void DieHandler(IDamageable enemy)
		{
			enemy.OnDie -= DieHandler;

			var unboxedEnemy = (Enemy) enemy;
			CurrentMoney += unboxedEnemy.GetMoneyForKill();

			unboxedEnemy.Release();

			_characterSpawner.RemoveInMonsterList(unboxedEnemy);

			if (_characterSpawner.MonstersInLevel.Count > 0) return;

			OnAllMonstersDied?.Invoke();
		}
	}
}