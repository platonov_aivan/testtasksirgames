﻿using Cinemachine.Utility;
using Extensions;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Gameplay.LevelGrid
{
    public class FieldGrid : MonoBehaviour
    {
	    [Title("Monsters")]
        [SerializeField, GroupComponent] private Transform _corner1;
        [SerializeField, GroupComponent] private Transform _corner2;
        [Title("Player")]
        [SerializeField, GroupComponent] private Transform _playerSpawnPos;

        public Vector3 PlayerSpawnPos => _playerSpawnPos.position;

        public Vector3 GetRandomPos()
        {
            var corner1Pos = _corner1.position;
            var corner2Pos = _corner2.position;

            var spawnPos = new Vector3(
                Random.Range(corner1Pos.x, corner2Pos.x),
                0,
                Random.Range(corner1Pos.z, corner2Pos.z));
            
            return spawnPos;
        }
        

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;

            var centerPos = (_corner2.position + _corner1.position) / 2f;
            centerPos.y += 0.01f;

            var size = (_corner2.position - _corner1.position).Abs();

            Gizmos.DrawCube(centerPos, size);
        }
    }
}