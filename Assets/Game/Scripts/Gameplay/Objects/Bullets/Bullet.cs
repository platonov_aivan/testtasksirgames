﻿using Common.ObjectPool;
using Game.ScriptableObjects.Classes.Weapons;
using Game.Scripts.Gameplay.Interfaces;
using Game.Scripts.Gameplay.Obstacles;
using Game.Scripts.Gameplay.Providers;
using UnityEngine;
using static Common.Enums;

namespace Game.Scripts.Gameplay.Objects.Bullets
{
	public class Bullet : PoolItem
	{
		[SerializeField] private Transform _model;
		[SerializeField] private Rigidbody _bulletRb;

		private WeaponData _weaponData;
		private Vector3 _direction;
		private TeamType _teamType;


		public void Init(WeaponData weaponData, Transform target, TeamType teamType)
		{
			_weaponData = weaponData;
			_teamType = teamType;
			_direction = (target.position - _model.position).normalized;
			transform.LookAt(target);
		}

		public override void Release(bool disableObject = true)
		{
			base.Release(disableObject);
			_bulletRb.velocity = Vector3.zero;
		}

		private void FixedUpdate()
		{
			_bulletRb.AddForce(_direction * _weaponData.BulletSpeed, ForceMode.VelocityChange);
			transform.LookAt(transform.position + _direction);
		}

		private void OnCollisionEnter(Collision other)
		{
			if (other.transform.TryGetComponent(out IDamageable enemy))
			{
				if (enemy.Team != _teamType) enemy.TakeDamage(_weaponData.Damage);

				SpawnParticle();
				Release();
				return;
			}

			if (other.transform.TryGetComponent(out Obstacle _))
			{
				Release();
			}
		}

		private void SpawnParticle()
		{
			var pooledParticle =
				PrefabProvider.GetParticlePrefab(_weaponData.BulletParticle);
			Pool.Get(pooledParticle, _model.position);
		}
	}
}