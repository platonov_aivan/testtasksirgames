﻿using System;
using DG.Tweening;
using Extensions;
using Game.Scripts.Gameplay.Characters.Player;
using UnityEngine;

namespace Game.Scripts.Gameplay.Objects
{
	public class NextLevelDoor : MonoBehaviour
	{
		public event Action OnEnterDoor;

		[SerializeField, GroupComponent] private Transform _doorModel;
		[SerializeField, GroupSetting] private float _valueY = 3f;
		[SerializeField, GroupSetting] private float _openDoorDuration = 0.5f;

		public void OpenDoor()
		{
			_doorModel.DOKill();
			_doorModel.DOLocalMoveY(_doorModel.position.y + _valueY, _openDoorDuration).SetLink(gameObject);
		}

		private void OnTriggerEnter(Collider other)
		{
			if (other.TryGetComponent(out Player _))
			{
				OnEnterDoor?.Invoke();
			}
		}
	}
}