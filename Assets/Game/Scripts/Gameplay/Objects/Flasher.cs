using DG.Tweening;
using Extensions;
using UnityEngine;

namespace Gameplay.Objects
{
	public class Flasher : MonoBehaviour
	{
		[SerializeField, GroupComponent(false)] private MeshRenderer        _meshRenderer;
		[SerializeField, GroupComponent(false)] private SkinnedMeshRenderer _skinnedMeshRenderer;

		[SerializeField, GroupSetting] private Color _flashColor    = Color.white;
		[SerializeField, GroupSetting] private float _flashDuration = 0.2f;

		private static readonly int emissionColor = Shader.PropertyToID("_EmissionColor");

		public void DoFlash()
		{
			if (_meshRenderer != default) Flash(_meshRenderer.sharedMaterial);
			if (_skinnedMeshRenderer != default) Flash(_skinnedMeshRenderer.sharedMaterial);
		}

		private void Flash(Material material)
		{
			material.DOKill();
			material.DOColor(_flashColor, emissionColor, _flashDuration)
							.OnComplete(() => material.DOColor(Color.clear, emissionColor, _flashDuration));
		}
	}
}