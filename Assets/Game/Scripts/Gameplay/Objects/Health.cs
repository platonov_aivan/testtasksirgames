using System;
using System.Collections;
using Extensions;
using UnityEngine;

namespace Gameplay
{
	public class Health : MonoBehaviour
	{
		public event Action OnFull;
		public event Action OnEmpty;
		public event Action<float> OnChange;
		public event Action<float> OnInit;

		[GroupView] private float _maxHealth;
		[GroupView] private float _currentHealth;
		[GroupView] private float _regenerationHealth;
		[GroupView] private float _regenerationDelay;

		private Coroutine _regenerationCor;

		public float CurrentHealth => _currentHealth;

		public float Percent => _currentHealth / _maxHealth;
		public bool IsEmpty => _currentHealth <= 0 && _currentHealth < _maxHealth;
		public bool IsFull => _currentHealth >= _maxHealth;

		public void Init(float maxHealth, float regenerationHealth, float regenerationDelay)
		{
			_currentHealth = _maxHealth = maxHealth;
			_regenerationHealth = regenerationHealth;
			_regenerationDelay = regenerationDelay;
			OnInit?.Invoke(_maxHealth);
		}

		public void Increase(float value)
		{
			_currentHealth = Mathf.Clamp(_currentHealth + value, 0, _maxHealth);
			OnChange?.Invoke(_currentHealth);
			if (!IsFull) return;

			OnFull?.Invoke();
			_regenerationCor.Stop(this);
		}

		public void Decrease(float value)
		{
			_currentHealth = Mathf.Clamp(_currentHealth - value, 0, _maxHealth);
			OnChange?.Invoke(_currentHealth);
			if (IsEmpty)
				OnEmpty?.Invoke();
			else
				Regenerate();
		}

		private void Regenerate()
		{
			if (_regenerationHealth == 0) return;

			_regenerationCor.Stop(this);
			_regenerationCor = StartCoroutine(RegenerationCor());
		}

		private IEnumerator RegenerationCor()
		{
			while (true)
			{
				yield return new WaitForSeconds(_regenerationDelay);

				Increase(_regenerationHealth);
			}
		}

		public void Empty()
		{
			_currentHealth = 0;
			OnChange?.Invoke(_currentHealth);
			OnEmpty?.Invoke();
		}

		public void Full()
		{
			_currentHealth = _maxHealth;
			OnChange?.Invoke(_currentHealth);
			OnFull?.Invoke();
		}
	}
}