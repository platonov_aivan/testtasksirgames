﻿using System;
using UnityEngine;
using static Common.Enums;

namespace Game.Scripts.Gameplay.Interfaces
{
	public interface IDamageable
	{
		public event Action<IDamageable> OnDie;
		public TeamType Team { get; }
		public Transform Model { get; }
		public Transform ShootTarget { get; }
		public Vector3 ModelPosition { get; }
		public void TakeDamage(float value);
	}
}