﻿using System;
using System.Collections.Generic;
using Common.ObjectPool;
using Extensions;
using Game.Scripts.Gameplay.Characters;
using Game.Scripts.Gameplay.Characters.Enemies;
using Sirenix.OdinInspector;
using UnityEngine;
using static Common.Enums;
using PrefabProvider = Game.Scripts.Gameplay.Providers.PrefabProvider;

namespace Game.Scripts.Gameplay.Spawner
{
	public class CharacterSpawner : MonoBehaviour
	{
		[Title("Player")]
		[SerializeField, GroupSetting] private TeamType _playerTeamType;
		[SerializeField, GroupSetting] private CharacterType _playerType;
		[Title("Monsters")]
		[SerializeField, GroupSetting] private TeamType _monsterTeamType;
		[SerializeField, GroupSetting] private List<CharacterType> _characterTypes;

		[GroupView] private CharacterBase _playerInLevel;
		[GroupView] private readonly List<Enemy> _monstersInLevel = new List<Enemy>();

		public CharacterBase PlayerInLevel => _playerInLevel;

		public List<Enemy> MonstersInLevel => _monstersInLevel;

		public void SpawnMonsters(Func<Vector3> getRandomPos, int monsterCount)
		{
			for (var i = 0; i < monsterCount; i++)
			{
				var characterElement =
					PrefabProvider.GetCharacterPrefab(_monsterTeamType, _characterTypes.GetRandomElement());
				var monster = Pool.Get(characterElement.Prefab);

				//	if (monster is not Enemy enemy) continue;

				var enemy = monster as Enemy;
				if (enemy == default) continue;

				AddInMonsterList(enemy);

				enemy.SetRandomPos(getRandomPos);
				enemy.SetPlayer(_playerInLevel);
				enemy.Init(monster.Data);
				enemy.Warp(getRandomPos());
			}
		}

		public void RemoveInMonsterList(Enemy enemy)
		{
			if (_monstersInLevel.Contains(enemy))
			{
				_monstersInLevel.Remove(enemy);
			}
		}

		public void SpawnPlayer(Vector3 playerSpawnPos)
		{
			var characterElement = PrefabProvider.GetCharacterPrefab(_playerTeamType, _playerType);
			var player = Pool.Get(characterElement.Prefab, playerSpawnPos);

			_playerInLevel = player;
			player.Init(characterElement.Data);
		}

		private void AddInMonsterList(Enemy enemy)
		{
			if (!_monstersInLevel.Contains(enemy))
			{
				_monstersInLevel.Add(enemy);
			}
		}
	}
}