﻿using Game.Scripts.Levels;
using UnityEngine;

namespace Game.Scripts.Gameplay
{
	public class EntryPoint : MonoBehaviour
	{
		[SerializeField] private LevelLoader _levelLoader;

		private void Awake()
		{
			_levelLoader.LevelLoad();
		}
	}
}
