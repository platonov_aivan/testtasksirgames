using System;
using UnityEngine;
using Utils;

namespace Game.Scripts.Gameplay.Characters
{
	public class TouchInput : Singleton<TouchInput>
	{
		public static event Action OnTap;
		public static event Action OnRelease;

		[SerializeField] private Joystick _joystick;

		public static bool Active { get; set; } = true;
		public static Vector2 Axis => Active ? Instance._joystick.Direction : Vector2.zero;

		private void OnEnable()
		{
			_joystick.OnTap += JoystickOnTap;
			_joystick.OnRelease += () => OnRelease?.Invoke();
		}

		private void JoystickOnTap()
		{
			OnTap?.Invoke();
		}

		public static void ShowJoystick(bool value)
		{
			if (Instance == null || Instance._joystick.CGroup == null) return;

			Instance._joystick.CGroup.alpha = value ? 1 : 0;
			Instance._joystick.CGroup.interactable = value;
			Instance._joystick.CGroup.blocksRaycasts = value;
		}
	}
}