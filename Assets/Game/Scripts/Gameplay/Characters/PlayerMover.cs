using Extensions;
using UnityEngine;

namespace Game.Scripts.Gameplay.Characters
{
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField, GroupSetting] private bool _clampToMaxSpeed = true;
        [SerializeField, GroupComponent] private CharacterBase _character;

        public bool Active { get; set; } = true;

        private Transform _cameraTransform;

        private void Awake()
        {
            _cameraTransform = Camera.main.transform;
        }

        private void Update()
        {
            var input = TouchInput.Axis;

            var forward = Vector3.ProjectOnPlane(_cameraTransform.forward, Vector3.up).normalized;
            var right = Vector3.ProjectOnPlane(_cameraTransform.right, Vector3.up).normalized;
            
            var worldInput = forward * input.y + right * input.x;
            var direction = _clampToMaxSpeed ? worldInput.normalized : Vector3.ClampMagnitude(worldInput, input.magnitude);

            _character.Move(Active ? direction : Vector3.zero);
        }
    }
}