﻿using System;
using Extensions;
using Game.ScriptableObjects.Classes.Characters;
using Game.Scripts.Gameplay.Characters.Enemies.EnemyLogicState;
using Gameplay.Characters;
using UnityEngine;
using UnityEngine.AI;
using static Common.Enums;

namespace Game.Scripts.Gameplay.Characters.Enemies
{
	public class Enemy : CharacterBase
	{
		[SerializeField] private NavMeshAgent _agent;
		[GroupView] private string CurrentState => _stateMachine?.NameOfState;
		[GroupView] private ActionType _currentAction;

		private readonly RaycastHit[] _raycastHits = new RaycastHit[10];
		private StateMachine _stateMachine;
		private CharacterBase _player;
		private Func<Vector3> _getRandomPos;
		private float _sqrDistanceToPlayer;
		private bool _isClosePlayer;


		public override void Init(CharacterData data)
		{
			base.Init(data);

			_currentAction = ActionType.Idle;
			_sqrDistanceToPlayer = Mathf.Pow(Data.DetectRadius, 2);
			_agent.speed = data.Speed;

			InitStateMachine();
		}

		public void StartAction()
		{
			_currentAction = Data.ActionType;
		}

		public void Warp(Vector3 position)
		{
			NavMesh.SamplePosition(position, out var hit, 30f, _agent.areaMask);
			var hitPos = new Vector3(hit.position.x, hit.position.y, hit.position.z);
			_agent.Warp(hitPos);
		}

		public void SetRandomPos(Func<Vector3> getRandomPos)
		{
			_getRandomPos = getRandomPos;
		}

		private void InitStateMachine()
		{
			_stateMachine = new StateMachine();

			var idleBaseState = new IdleBaseState(this);
			var randomMoveState = new RandomMoveState(this, _agent, _getRandomPos);
			var moveToPlayerState = new MoveToPlayerState(this, _agent, _player);
			var attackState = new AttackState(this, _player);


			_stateMachine.AddTransition(idleBaseState, randomMoveState, IsRandomMove);
			_stateMachine.AddTransition(idleBaseState, moveToPlayerState, IsMoveToPlayer);

			_stateMachine.AddTransition(randomMoveState, moveToPlayerState, IsMoveToPlayer);
			_stateMachine.AddTransition(moveToPlayerState, randomMoveState, IsRandomMove);

			_stateMachine.AddTransition(randomMoveState, idleBaseState, IsIdle);
			_stateMachine.AddTransition(moveToPlayerState, idleBaseState, IsIdle);

			_stateMachine.AddTransition(idleBaseState, idleBaseState, IsIdle);
			_stateMachine.AddTransition(moveToPlayerState, idleBaseState, IsIdle);

			_stateMachine.AddTransition(randomMoveState, attackState, IsAttack);
			_stateMachine.AddTransition(moveToPlayerState, attackState, IsAttack);

			_stateMachine.AddTransition(attackState, randomMoveState, IsNotAttackAndRandomMove);
			_stateMachine.AddTransition(attackState, moveToPlayerState, IsNotAttackAndMoveToPlayer);

			bool IsRandomMove() => _currentAction == ActionType.RandomMove;
			bool IsMoveToPlayer() => _currentAction == ActionType.MoveToPlayer;
			bool IsIdle() => _currentAction == ActionType.Idle;
			bool IsAttack() => _isClosePlayer;
			bool IsNotAttackAndRandomMove() => !_isClosePlayer && IsRandomMove();
			bool IsNotAttackAndMoveToPlayer() => !_isClosePlayer && IsMoveToPlayer();

			_stateMachine.SetState(idleBaseState);
		}

		public int GetMoneyForKill()
		{
			return Data.RewardMoney;
		}

		public void SetPlayer(CharacterBase player)
		{
			_player = player;
		}

		private void Update()
		{
			_stateMachine.Tick();

			CheckPlayer();
		}

		private void OnCollisionEnter(Collision other)
		{
			if (other.transform.TryGetComponent(out Player.Player player))
			{
				player.TakeDamage(Data.CollisionDamage);
			}
		}

		private void CheckPlayer()
		{
			if (_player == default) return;

			var sqrDistanceTo = ModelPosition.SqrDistanceTo(_player.ModelPosition);

			if (sqrDistanceTo > _sqrDistanceToPlayer)
			{
				_isClosePlayer = false;
				return;
			}

			var ray = new Ray(transform.position, _player.ModelPosition - transform.position);

			var minDistance = float.MaxValue;
			RaycastHit hit = default;
			var count = Physics.RaycastNonAlloc(ray, _raycastHits, Data.MaxDistance, Data.RayLayerMask);

			if (count <= 0) return;

			for (var i = 0; i < count; i++)
			{
				if (_raycastHits[i].distance < minDistance)
				{
					hit = _raycastHits[i];
					minDistance = hit.distance;
				}
			}
			
#if UNITY_EDITOR
			Debug.DrawRay(ray.origin, ray.direction * hit.distance);
#endif
			_isClosePlayer = hit.collider.TryGetComponent(out Player.Player _);
		}
	}
}