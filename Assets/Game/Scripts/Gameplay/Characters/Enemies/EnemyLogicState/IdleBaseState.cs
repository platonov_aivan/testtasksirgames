﻿namespace Game.Scripts.Gameplay.Characters.Enemies.EnemyLogicState
{
    public class IdleBaseState : IState
    {
        private readonly CharacterBase _characterBase;

        public IdleBaseState(CharacterBase characterBase)
        {
            _characterBase = characterBase;
        }

        public void OnEnter()
        {
        }

        public void Tick()
        {
        }

        public void OnExit()
        {
        }
    }
}