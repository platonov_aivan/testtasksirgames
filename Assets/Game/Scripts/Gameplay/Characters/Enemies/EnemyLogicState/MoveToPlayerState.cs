﻿using Extensions;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Scripts.Gameplay.Characters.Enemies.EnemyLogicState
{
	public class MoveToPlayerState : IState
	{
		private readonly NavMeshAgent _navMeshAgent;
		private readonly CharacterBase _characterBase;
		private readonly CharacterBase _player;
		private Vector3 _currentTargetPosition;

		public MoveToPlayerState(CharacterBase characterBase, NavMeshAgent navMeshAgent, CharacterBase player)
		{
			_characterBase = characterBase;
			_navMeshAgent = navMeshAgent;
			_player = player;
		}

		public void OnEnter()
		{
			_navMeshAgent.isStopped = false;
			
			SetDestination();
		}

		public void Tick()
		{
			if (!_navMeshAgent.IsReached() && _navMeshAgent.path.corners.Length > 1)
			{
				var pathCorner = _navMeshAgent.path.corners[1];
				_characterBase.transform.HorizontalSoftLookAt(pathCorner);
			}
			else
			{
				SetDestination();
			}
		}
		
		private void SetDestination()
		{
			if (!_navMeshAgent.enabled) return;
			_currentTargetPosition = _player.ModelPosition.XZOnly();
			_characterBase.Move(_currentTargetPosition);
		}

		public void OnExit()
		{
			_navMeshAgent.isStopped = true;
		}
	}
}
