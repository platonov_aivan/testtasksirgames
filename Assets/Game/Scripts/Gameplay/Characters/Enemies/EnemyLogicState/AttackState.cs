﻿using Common.ObjectPool;
using Extensions;
using Game.Scripts.Gameplay.Providers;
using UnityEngine;

namespace Game.Scripts.Gameplay.Characters.Enemies.EnemyLogicState
{
	public class AttackState : IState
	{
		private readonly CharacterBase _characterBase;
		private readonly CharacterBase _player;
		private float _lastShotTime;

		public AttackState(CharacterBase characterBase, CharacterBase player)
		{
			_characterBase = characterBase;
			_player = player;
		}

		public void OnEnter()
		{
		}

		public void Tick()
		{
			_characterBase.transform.HorizontalSoftLookAt(_player.Model, _characterBase.Data.ShootRotationSpeed);
			
			if (_characterBase.transform.Dot(_player.Model) < _player.Data.DotForShoot) return;

			if (_lastShotTime + _characterBase.WeaponData.ShootDelay > Time.time) return;

			_lastShotTime = Time.time;

			Shoot(_player.ShootTarget);
		}

		public void OnExit()
		{
		}

		private void Shoot(Transform target)
		{
			var bulletPrefab = PrefabProvider.GetBulletPrefab(_characterBase.WeaponData.BulletType);
			var bullet = Pool.Get(bulletPrefab, _characterBase.BulletSpawnPos.position);
			bullet.Init(_characterBase.WeaponData, target, _characterBase.Team);
		}
	}
}