﻿using System;
using Extensions;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Scripts.Gameplay.Characters.Enemies.EnemyLogicState
{
	public class RandomMoveState : IState
	{
		private readonly CharacterBase _characterBase;
		private readonly NavMeshAgent _navMeshAgent;
		private readonly Func<Vector3> _getRandomPos;
		private Vector3 _currentTargetPosition;
		private float _currentImmobilityTime;

		public RandomMoveState(CharacterBase characterBase, NavMeshAgent navMeshAgent, Func<Vector3> getRandomPos)
		{
			_characterBase = characterBase;
			_navMeshAgent = navMeshAgent;
			_getRandomPos = getRandomPos;
		}

		public void OnEnter()
		{
			_currentImmobilityTime = 0f;
			_navMeshAgent.isStopped = false;
			
			
			SetDestination();
		}

		public void Tick()
		{
			if (_navMeshAgent.IsReached())
			{
				if (_currentImmobilityTime <= _characterBase.Data.ImmobilityTime)
				{
					_currentImmobilityTime += Time.deltaTime;
				}
				else
				{
					_currentImmobilityTime = 0f;
					SetDestination();
				}
			}
			else
				_characterBase.transform.HorizontalSoftLookAt(_currentTargetPosition);
		}

		public void OnExit()
		{
			_navMeshAgent.isStopped = true;
		}

		private void SetDestination()
		{
			if (!_navMeshAgent.enabled) return;
			_currentTargetPosition = _getRandomPos();
			_characterBase.Move(_currentTargetPosition);
		}
	}
}
