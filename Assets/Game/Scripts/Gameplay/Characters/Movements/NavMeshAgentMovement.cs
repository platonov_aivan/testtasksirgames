using Extensions;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Scripts.Gameplay.Characters.Movements
{
	[RequireComponent(typeof(NavMeshAgent))]
	public class NavMeshAgentMovement : MovementBehaviour
	{
		[SerializeField, GroupComponent] private CharacterAnimator _animator;
		[SerializeField, GroupComponent] private NavMeshAgent      _agent;
		[SerializeField, GroupComponent] private Rigidbody         _rigidbody;

		[GroupView] private float Velocity => _agent == default ? 0 : _agent.velocity.magnitude;
		[GroupView] private float DesiredVelocity => _agent == default ? 0 : _agent.desiredVelocity.magnitude;

		public override bool IsStopped => _agent.velocity.sqrMagnitude == 0;
		public override Vector3 CurrentVelocity => _agent.velocity;

		public override void Move(Vector3 input)
		{
			if (_agent.enabled && _agent.isOnNavMesh) _agent.SetDestination(input);
		}

		private void Update()
		{
			if (_animator == default) return;
			
			if (_agent.isStopped)
			{
				 _animator.SetSpeed(_rigidbody.velocity.XZOnly().magnitude);
			}
			else
				_animator.SetSpeed(_agent.desiredVelocity.magnitude);
		}

		public override void Warp(Vector3 input)
		{
			if (_agent.enabled && _agent.isOnNavMesh) _agent.Warp(input);
		}

		public override void Stop()
		{
			base.Stop();
			if (_agent.enabled) _agent.isStopped = true;
		}

		public override void Resume()
		{
			base.Resume();
			if (_agent.enabled && _agent.isOnNavMesh) _agent.isStopped = false;
		}

		public override void Enable()
		{
			base.Enable();
			_agent.enabled = true;
		}

		public override void Disable()
		{
			if (_agent.enabled && _agent.isOnNavMesh) _agent.ResetPath();
			_agent.enabled = false;
			if (_animator != default) _animator.SetSpeed(0);
			base.Disable();
		}
	}
}