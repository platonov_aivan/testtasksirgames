﻿using UnityEngine;

namespace Game.Scripts.Gameplay.Characters.Movements
{
	public class NoMovement : MovementBehaviour
	{
		public override void Move(Vector3 input)
		{
		}

		public override void Warp(Vector3 input)
		{
		}

		public override bool IsStopped => true;
	}
}