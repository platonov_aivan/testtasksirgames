using System;
using UnityEngine;

namespace Game.Scripts.Gameplay.Characters
{
	[RequireComponent(typeof(SphereCollider))]
	public class InteractionZone : MonoBehaviour
	{
		public event Action<Collider> OnZoneEnter;
		public event Action<Collider> OnZoneExit;

		[SerializeField] private SphereCollider _collider;
		public float Radius => _collider.radius;

		public void Init(float radius)
		{
			_collider.radius = radius;
		}

		private void OnTriggerEnter(Collider other)
		{
			OnZoneEnter?.Invoke(other);
		}

		private void OnTriggerExit(Collider other)
		{
			OnZoneExit?.Invoke(other);
		}
	}
}