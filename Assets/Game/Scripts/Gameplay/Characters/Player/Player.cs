using System.Collections;
using DG.Tweening;
using Extensions;
using Game.ScriptableObjects.Classes.Characters;
using Game.Scripts.Gameplay.Shooting;
using UnityEngine;

namespace Game.Scripts.Gameplay.Characters.Player
{
	public class Player : CharacterBase
	{
		[SerializeField] private ShootComponent _shootComponent;

		private Coroutine _shotCor;
		private float _lastShotTime;

		public override void Init(CharacterData data)
		{
			base.Init(data);

			transform.rotation = Quaternion.identity;
		}

		public override void InitMovement()
		{
			base.InitMovement();

			EnableKinematicBody(false);
			EnableInput(true);
		}

		public override void Restart()
		{
			base.Restart();

			EnableInput(false);
			_shootComponent.ClearEnemy();
		}

		private void Awake()
		{
			EnableInput(false);

			_shootComponent.OnStartShoot += StartShoot;
			_shootComponent.OnStopShoot += StopShoot;
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
			_shootComponent.OnStartShoot -= StartShoot;
			_shootComponent.OnStopShoot -= StopShoot;
		}

		protected override void Die()
		{
			base.Die();
			Release();
		}

		#region Input

		private void EnableInput(bool active)
		{
			TouchInput.Active = active;
			TouchInput.ShowJoystick(active);
		}

		#endregion

		#region Shoot

		private void StartShoot()
		{
			StopShoot();
			_shotCor = StartCoroutine(ShotCor());
		}

		private IEnumerator ShotCor()
		{
			while (true)
			{
				yield return null;

				if (!_movement.IsStopped) continue;

				var closeEnemy = _shootComponent.GetCloseEnemy();

				if (closeEnemy == default)
				{
					ResetModelRotation();
					continue;
				}

				transform.HorizontalSoftLookAt(closeEnemy.Model, Data.ShootRotationSpeed);


				if (transform.Dot(closeEnemy.Model) < Data.DotForShoot) continue;

				if (_lastShotTime + _weaponData.ShootDelay > Time.time) continue;

				_lastShotTime = Time.time;

				_shootComponent.Shoot(closeEnemy.ShootTarget);
			}
		}
		
		
		private void ResetModelRotation()
		{
			if (_model.localRotation == Quaternion.identity) return;

			_model.DOKill();
			_model.DOLocalRotate(Vector3.zero, 0.3f).SetLink(gameObject);
		}


		private void StopShoot()
		{
			_shotCor.Stop(this);
			_shotCor = default;

			ResetModelRotation();
		}

		#endregion
	}
}