using System;
using Common.ObjectPool;
using Extensions;
using Game.ScriptableObjects.Classes.Characters;
using Game.ScriptableObjects.Classes.Weapons;
using Game.Scripts.Gameplay.Characters.Movements;
using Game.Scripts.Gameplay.Interfaces;
using Gameplay;
using Gameplay.Objects;
using Sirenix.OdinInspector;
using UnityEngine;
using static Common.Enums;

namespace Game.Scripts.Gameplay.Characters
{
	[SelectionBase]
	public class CharacterBase : PoolItem, IDamageable
	{
		public event Action<IDamageable> OnDie;

		[SerializeField, GroupComponent] protected Transform         _model;
		[SerializeField, GroupComponent] protected Transform         _shootTarget;
		[SerializeField, GroupComponent] protected MovementBehaviour _movement;
		[SerializeField, GroupComponent] protected Rigidbody         _body;
		[SerializeField, GroupComponent] protected CharacterAnimator _animator;
		[SerializeField, GroupComponent] protected Health            _health;
		[SerializeField, GroupComponent] protected Flasher           _flasher;
		[SerializeField, GroupComponent] protected InteractionZone   _farZone;
		[SerializeField, GroupComponent] protected Transform         _bulletSpawnPos;
		
		[SerializeField, GroupSetting, AssetList] private CharacterData _characterData;
		[SerializeField, GroupSetting, AssetList] protected WeaponData _weaponData;
		
		
		[GroupView] public bool IsDead => _health != default && _health.IsEmpty;
		[GroupView] public TeamType Team => Data == null ? TeamType.None : Data.Team;
		
		public Transform Model => _model;

		public Transform ShootTarget => _shootTarget;

		public Transform BulletSpawnPos => _bulletSpawnPos;
		public Vector3 ModelPosition => _model.position;
		public CharacterData Data => _characterData;
		public WeaponData WeaponData => _weaponData;
		public Rigidbody Body => _body;
		public CharacterAnimator Animator => _animator;

		public InteractionZone FarZone => _farZone;

		protected virtual void Start()
		{
			InitHealth();
			InitZones();
		}

		public override void Restart()
		{
			base.Restart();
			InitHealth();
		}

		protected virtual void OnEnable() => Subscribe();
		protected virtual void OnDisable() => Unsubscribe();

		protected virtual void OnDestroy() { }

		#region Init

		public virtual void Init(CharacterData data) => _characterData = data;

		public virtual void InitMovement()
		{
			if (_movement != default) _movement.Enable();
		}

		private void InitZones() => _farZone.Init(Data.DetectRadius);

		private void InitHealth() => _health.Init(Data.MaxHealth, Data.RegenerationHealth, Data.RegenerationDelay);

		private void Subscribe() => _health.OnEmpty += Die;

		private void Unsubscribe() => _health.OnEmpty -= Die;

		#endregion

		public void Move(Vector3 input)
		{
			_movement.Move(input);
		}

		public void TakeDamage(float value)
		{
			if (IsDead) return;

			_health.Decrease(value);
			if (!IsDead) _flasher.DoFlash();
		}
		
		[Button(ButtonSizes.Large), GUIColor(0, 1, 0)]
		protected virtual void Die()
		{
			if (_movement != default) _movement.Disable();
			if (_animator != default) _animator.DoDie(true);
			EnableKinematicBody(true);
			OnDie?.Invoke(this);
			AfterDie();
		}

		protected void EnableKinematicBody(bool active)
		{
			_body.isKinematic = false;
		}

		protected virtual void AfterDie()
		{
		}
	}
}