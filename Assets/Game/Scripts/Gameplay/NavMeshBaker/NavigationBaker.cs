using System;
using System.Collections;
using System.Collections.Generic;
using Extensions;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Scripts.Gameplay.NavMeshBaker
{
	public class NavigationBaker : MonoBehaviour
	{
		public event Action OnBuildNavmesh;
		
		[SerializeField] private NavMeshSurface _surface;
		[SerializeField] private NavMeshSurface _flySurface;

		private Coroutine _navmeshBakeCor;

		public void InitNavmesh()
		{
			_navmeshBakeCor.Stop(this);
			_navmeshBakeCor = StartCoroutine(BakeNavmesh());
		}
		
		private IEnumerator BakeNavmesh()
		{
			NavMesh.RemoveAllNavMeshData();
			yield return null;
			
			_surface.BuildNavMesh();
			_flySurface.BuildNavMesh();
			yield return null;
			
			OnBuildNavmesh?.Invoke();
		}

		private IEnumerator BuildNavmesh(NavMeshSurface surface)
		{
			var data  = InitializeBakeData(surface);
			var async = surface.UpdateNavMesh(data);

			yield return async;

			Debug.Log("finished");

			surface.navMeshData = data;
			surface.AddData();
		}

		private NavMeshData InitializeBakeData(NavMeshSurface surface)
		{
			var emptySources = new List<NavMeshBuildSource>();
			var emptyBounds  = new Bounds();

			return NavMeshBuilder.BuildNavMeshData(surface.GetBuildSettings(), emptySources, emptyBounds,
																						 surface.transform.position, surface.transform.rotation);
		}
	}
}