﻿using Game.Scripts.Levels;
using Game.Scripts.UI.View;
using UnityEngine;

namespace Game.Scripts.Gameplay.Mediators
{
	public class GameMediator : MonoBehaviour
	{
		[SerializeField] private LevelLoader _levelLoader;
		[SerializeField] private GameView _gameView;
		[SerializeField] private PauseView _pauseView;

		private void Awake()
		{
			_levelLoader.OnLevelLoaded += Init;
			_levelLoader.OnLevelDestroy += UnsubscribeLevel;

			_gameView.PauseButton.onClick.AddListener(OpenPauseMenu);
			_pauseView.ExitPauseButton.onClick.AddListener(ClosePauseMenu);
		}

		private void OnDestroy()
		{
			_levelLoader.OnLevelLoaded -= Init;
			_levelLoader.OnLevelDestroy -= UnsubscribeLevel;

			_gameView.PauseButton.onClick.RemoveListener(OpenPauseMenu);
			_pauseView.ExitPauseButton.onClick.RemoveListener(ClosePauseMenu);
		}

		private void Init(Level currentLevel, int levelIndex)
		{
			_gameView.EnableTimeText(true);
			_gameView.SetLevelText(levelIndex);
			_gameView.SetMoneyText(currentLevel.GameSession.CurrentMoney);
			_gameView.SetTimerText(currentLevel.GameSession.StartTime);

			currentLevel.GameSession.OnTimerChanged += SetTimerText;
			currentLevel.GameSession.OnMoneyChanged += SetMoneyText;
			currentLevel.GameSession.OnStartBattle += StartBattle;
		}

		private void UnsubscribeLevel(Level currentLevel)
		{
			currentLevel.GameSession.OnTimerChanged -= SetTimerText;
			currentLevel.GameSession.OnMoneyChanged -= SetMoneyText;
			currentLevel.GameSession.OnStartBattle -= StartBattle;
		}

		private void StartBattle()
		{
			_gameView.EnableTimeText(false);
		}

		private void SetMoneyText(int currentMoney)
		{
			_gameView.SetMoneyText(currentMoney);
		}

		private void SetTimerText(float time)
		{
			_gameView.SetTimerText(time);
		}

		private void OpenPauseMenu()
		{
			_pauseView.OpenPauseMenu(() => Time.timeScale = 0f);
		}

		private void ClosePauseMenu()
		{
			_pauseView.ClosePauseMenu(() => Time.timeScale = 1f);
		}
	}
}