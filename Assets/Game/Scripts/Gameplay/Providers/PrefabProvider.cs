﻿using System;
using Common.ObjectPool;
using Game.ScriptableObjects.Classes.Prefabs;
using Game.Scripts.Gameplay.Objects.Bullets;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;
using static Common.Enums;

namespace Game.Scripts.Gameplay.Providers
{
	public class PrefabProvider : Singleton<PrefabProvider>
	{
		[SerializeField, AssetList] private PrefabData _prefabData;

		public static PooledParticle GetParticlePrefab(ParticleType type)
		{
			foreach (var element in Instance._prefabData.ParticlePrefabs)
			{
				if (element.Type == type) return element.Prefab;
			}
			throw new NullReferenceException($"Check Particle Prefab Data {type}");
			
		}
		public static Bullet GetBulletPrefab(BulletType type)
		{
			foreach (var element in Instance._prefabData.BulletsPrefabs)
			{
				if (element.Type == type) return element.Prefab;
			}
			throw new NullReferenceException($"Check Bullet Prefab Data {type}");

		}

		public static CharacterElement GetCharacterPrefab(TeamType team, CharacterType type)
		{
			foreach (var element in Instance._prefabData.CharacterPrefabs)
			{
				if (element.Team == team && element.Type == type) return element;
			}

			throw new NullReferenceException($"Check Character Prefab Data {type} {team}");
		}
	}
}