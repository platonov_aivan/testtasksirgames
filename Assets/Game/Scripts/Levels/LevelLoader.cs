﻿using System;
using System.Collections.Generic;
using Common.ObjectPool;
using UnityEngine;

namespace Game.Scripts.Levels
{
    public class LevelLoader : MonoBehaviour
    {
	    public event Action<Level, int> OnLevelLoaded;
	    public event Action<Level> OnLevelDestroy;
	    
        [SerializeField] private Transform _world;
        [SerializeField] private List<Level> _levels;

        private int _currentLevelIndex;

        public void LevelLoad()
        {
	        Pool.ReleaseAll();
            if (_currentLevelIndex >= _levels.Count) _currentLevelIndex = 0;
            var currentLevel = Instantiate(_levels[_currentLevelIndex], _world);
            currentLevel.OnLevelLoaded += LevelLoaded;
            currentLevel.OnLevelComplete += NextLevel;
            currentLevel.OnLevelLose += RetryLevel;
            currentLevel.Init();
        }

        private void LevelLoaded(Level currentLevel)
        {
	        currentLevel.OnLevelLoaded -= LevelLoaded;
	        
	        OnLevelLoaded?.Invoke(currentLevel, _currentLevelIndex);
        }

        private void NextLevel(Level currentLevel)
        {
	        currentLevel.OnLevelComplete -= NextLevel;
	        _currentLevelIndex++;
	        
	        OnLevelDestroy?.Invoke(currentLevel);
	        Destroy(currentLevel.gameObject);
            
	        LevelLoad();
        }

        private void RetryLevel(Level currentLevel)
        {
            currentLevel.OnLevelLose -= RetryLevel;
            
            OnLevelDestroy?.Invoke(currentLevel);
            Destroy(currentLevel.gameObject);
            
            LevelLoad();
        }
    }
}