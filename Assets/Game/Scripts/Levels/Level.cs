﻿using System;
using Cinemachine;
using Extensions;
using Game.Scripts.Gameplay;
using Game.Scripts.Gameplay.NavMeshBaker;
using Game.Scripts.Gameplay.Objects;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Levels
{
	public class Level : MonoBehaviour
	{
		public static event Action<CinemachineTargetGroup> OnTargetGroupFollow;
		public event Action<Level> OnLevelLoaded;
		public event Action<Level> OnLevelComplete;
		public event Action<Level> OnLevelLose;

		[SerializeField, GroupComponent] private GameSession _gameSession;
		[SerializeField, GroupComponent] private NavigationBaker _navigationBaker;
		[SerializeField, GroupComponent] private NextLevelDoor _nextLevelDoor;
		[SerializeField, GroupComponent] private CinemachineTargetGroup _cinemachineTargetGroup;

		public GameSession GameSession => _gameSession;

		public void Init()
		{
			InitNavmesh();
			OnLevelLoaded?.Invoke(this);
			OnTargetGroupFollow?.Invoke(_cinemachineTargetGroup);
		}

		private void Awake()
		{
			_nextLevelDoor.OnEnterDoor += LevelWin;
			_gameSession.OnPlayerDied += LevelLose;
			_gameSession.OnAllMonstersDied += OpenLevelDoor;
		}

		private void OnDestroy()
		{
			_nextLevelDoor.OnEnterDoor -= LevelWin;
			_gameSession.OnPlayerDied -= LevelLose;
			_gameSession.OnAllMonstersDied -= OpenLevelDoor;
		}

		private void OpenLevelDoor()
		{
			_nextLevelDoor.OpenDoor();
		}

		private void InitNavmesh()
		{
			_navigationBaker.OnBuildNavmesh += BakeNavmeshComplete;
			_navigationBaker.InitNavmesh();
		}

		private void BakeNavmeshComplete()
		{
			_navigationBaker.OnBuildNavmesh -= BakeNavmeshComplete;

			SpawnCharacters();
		}

		private void SpawnCharacters()
		{
			_gameSession.Spawn();
		}

		[Button]
		private void LevelWin()
		{
			OnLevelComplete?.Invoke(this);
		}

		[Button]
		private void LevelLose()
		{
			OnLevelLose?.Invoke(this);
		}
	}
}