using System;
using System.Linq;
using Common;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(UniqueID))]
public class UniqueIDEditor : Editor
{
	private Scene _scene;

	private void OnEnable()
	{
		var uniqueID = (UniqueID) target;
		_scene = uniqueID.gameObject.scene;
		if (_scene.name == null) return;
		if (string.IsNullOrEmpty(uniqueID.ID) || !uniqueID.ID.StartsWith(_scene.name)) { Generate(uniqueID); }
		else
		{
			var uniqueIDs = FindObjectsOfType<UniqueID>();

			if (uniqueIDs.Any(other => other != uniqueID && other.ID == uniqueID.ID)) Generate(uniqueID);
		}
	}

	private void Generate(UniqueID uniqueID)
	{
		if (Application.isPlaying) return;

		uniqueID.ID = $"{_scene.name}_{Guid.NewGuid().ToString()}";
		EditorUtility.SetDirty(uniqueID);
		EditorSceneManager.MarkSceneDirty(_scene);
	}
}