﻿using Extensions;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using static Common.Enums;

namespace Game.ScriptableObjects.Classes.Characters
{
	[CreateAssetMenu(fileName = "CharacterData", menuName = "Characters/CharacterData", order = 0)]
	public class CharacterData : ScriptableObject
	{
		[SerializeField] private CharacterType _type;
		[SerializeField] private TeamType _team;
		[SerializeField, HideIf(nameof(IsPlayer))] private ActionType _actionType;

		[SerializeField, Group("Movement"), HideIf(nameof(IsPlayer))] private float _immobilityTime = 3;
		[SerializeField, Group("Movement")] private float _speed = 6;


		[SerializeField, Group("Movement"), ShowIf(nameof(IsPlayer))]
		private float _turnSmoothDuration = 0.1f;

		[SerializeField, Group("Movement"), ShowIf(nameof(IsPlayer))]
		private float _slowDownDuration = 0.2f;

		[SerializeField, Group("Movement"), ShowIf(nameof(IsPlayer))] private float _gravityForce = 1;

		[SerializeField, Group("Characteristics")] private float _maxHealth = 100;
		[SerializeField, Group("Characteristics")] private float _regenerationHealth;
		[SerializeField, Group("Characteristics")] private float _regenerationDelay;
		
		[SerializeField, Group("Interaction"), HideIf(nameof(IsPlayer)), Min(0)]
		private float _collisionDamage = 2f;
		[SerializeField, Group("Interaction"), Min(0)]
		private float _detectRadius = 5f;
		[SerializeField, Group("RayCast")]
		private LayerMask _rayLayerMask;
		[SerializeField, Group("RayCast")]
		private float _maxDistance;

		[SerializeField, Group("Reward"), HideIf(nameof(IsPlayer))]
		private int _rewardMoney = 3;

		[SerializeField, Group("Shoot")]
		private float _shootRotationSpeed = 10f;
		[SerializeField, Group("Shoot")]
		private float _dotForShoot = 0.8f;

		private bool IsPlayer => Type == CharacterType.Player;
		public CharacterType Type => _type;
		public TeamType Team => _team;

		public ActionType ActionType => _actionType;

		public LayerMask RayLayerMask => _rayLayerMask;
		
		public float ImmobilityTime => _immobilityTime;

		public float DetectRadius => _detectRadius;
		public float MaxDistance => _maxDistance;
		public float CollisionDamage => _collisionDamage;
		public float Speed => _speed;
		public float TurnSmoothDuration => _turnSmoothDuration;
		public float ShootRotationSpeed => _shootRotationSpeed;
		public float DotForShoot => _dotForShoot;
		public float SlowDownDuration => _slowDownDuration;
		public float GravityForce => _gravityForce;
		public float MaxHealth => _maxHealth;
		public float RegenerationHealth => _regenerationHealth;
		public float RegenerationDelay => _regenerationDelay;
		public int RewardMoney => _rewardMoney;
#if UNITY_EDITOR
		[Button(ButtonSizes.Large), GUIColor(0, 1, 0)]
		private void ApplyName()
		{
			var assetPath = AssetDatabase.GetAssetPath(this);
			AssetDatabase.RenameAsset(assetPath, $"{_team.ToString()} - {_type.ToString()}");
			AssetDatabase.Refresh();
		}
#endif
	}
}