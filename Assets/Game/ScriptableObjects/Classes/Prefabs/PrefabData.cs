﻿using System;
using System.Collections.Generic;
using Common.ObjectPool;
using Game.ScriptableObjects.Classes.Characters;
using Game.Scripts.Gameplay.Characters;
using Game.Scripts.Gameplay.Objects.Bullets;
using Gameplay.Characters;
using Sirenix.OdinInspector;
using UnityEngine;
using static Common.Enums;

namespace Game.ScriptableObjects.Classes.Prefabs
{
	[CreateAssetMenu(fileName = "PrefabData", menuName = "Prefabs/PrefabData", order = 0)]
	public class PrefabData : SerializedScriptableObject
	{
		[SerializeField, TableList] private List<ParticleElement> _particlePrefabs;
		[SerializeField, TableList] private List<BulletElement> _bulletsPrefabs;

		[SerializeField, TableList] private List<CharacterElement> _characterPrefabs;
		
		public List<ParticleElement> ParticlePrefabs => _particlePrefabs;

		public List<BulletElement> BulletsPrefabs => _bulletsPrefabs;

		public List<CharacterElement> CharacterPrefabs => _characterPrefabs;
	}

	[Serializable]
	public class CharacterElement
	{
		[SerializeField] private TeamType      _team;
		[SerializeField] private CharacterType _type;
		[SerializeField] private CharacterBase _prefab;
		[SerializeField] private CharacterData _data;
		public TeamType Team => _team;
		public CharacterType Type => _type;
		public CharacterBase Prefab => _prefab;
		public CharacterData Data => _data;
	}
	
	[Serializable]
	public class ParticleElement
	{
		[SerializeField] private ParticleType      _type;
		[SerializeField] private PooledParticle _prefab;

		public ParticleType Type => _type;
		public PooledParticle Prefab => _prefab;
	}
	[Serializable]
	public class BulletElement
	{
		[SerializeField] private BulletType      _type;
		[SerializeField] private Bullet _prefab;

		public BulletType Type => _type;

		public Bullet Prefab => _prefab;
	}
}