﻿using UnityEngine;

namespace Game.ScriptableObjects.Classes.GameSession
{
	[CreateAssetMenu(fileName = "GameSessionData", menuName = "Game/SessionData", order = 0)]
	public class GameSessionData : ScriptableObject
	{
		[SerializeField] private int _startTime;
		[SerializeField] private int _countMonsters = 2;

		public int StartTime => _startTime;

		public int CountMonsters => _countMonsters;
	}
}
