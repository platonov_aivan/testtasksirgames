﻿using Extensions;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using static Common.Enums;

namespace Game.ScriptableObjects.Classes.Weapons
{
    [CreateAssetMenu(fileName = "WeaponData", menuName = "Weapon/WeaponData", order = 0)]
    public class WeaponData : ScriptableObject
    {
        [SerializeField, GroupSetting] private float _damage = 5;
        [SerializeField, GroupSetting] private float _shootDelay = 2;
        [SerializeField, GroupSetting] private float _bulletSpeed = 3;
        [SerializeField, GroupSetting] private BulletType _bulletType;
        [SerializeField, GroupSetting] private ParticleType _bulletParticle;


        public float Damage => _damage;

        public float ShootDelay => _shootDelay;

        public float BulletSpeed => _bulletSpeed;

        public BulletType BulletType => _bulletType;

        public ParticleType BulletParticle => _bulletParticle;
        
        
#if UNITY_EDITOR
        [Button(ButtonSizes.Large), GUIColor(0, 1, 0)]
        private void ApplyName()
        {
	        var assetPath = AssetDatabase.GetAssetPath(this);
	        AssetDatabase.RenameAsset(assetPath, $"{_bulletType.ToString()}");
	        AssetDatabase.Refresh();
        }
#endif
    }
}